extern crate firefox_bookmarks;
use firefox_bookmarks::parse_json;

fn main() {
	let data = parse_json(include_str!("bookmarks-sample.json")).unwrap();
	println!("{:#?}", data);
}