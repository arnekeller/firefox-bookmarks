#![recursion_limit = "512"]
extern crate serde_json;
use serde_json::Value;
extern crate chrono;
use chrono::NaiveDateTime;
#[macro_use] extern crate if_chain;

/// Either container or place
#[derive(Debug)]
pub enum Entity {
	Container(PlaceContainer),
	Place(Place)
}

/// x-moz-place-container
#[derive(Debug)]
pub struct PlaceContainer {
	pub guid: String,
	pub title: String,
	pub index: u64,
	pub date_added: NaiveDateTime,
	pub last_modified: NaiveDateTime,
	pub id: u64,
	pub root: Option<String>,
	pub children: Vec<Entity>
}

/// x-moz-place
#[derive(Debug)]
pub struct Place {
	pub guid: String,
	pub title: String,
	pub index: u64,
	pub date_added: NaiveDateTime,
	pub last_modified: NaiveDateTime,
	pub id: u64,
	pub annos: Option<Value>,
	pub uri: String
}

/// Only `Place`s and `PlaceContainer`s will be extracted, anything else is silently discarded.
pub fn parse_json(json: &str) -> Option<PlaceContainer> {
	get_container(&serde_json::from_str(json).ok()?)
}

fn get_container(json: &Value) -> Option<PlaceContainer> {
	if_chain! {
		if Some(Some("text/x-moz-place-container")) == json.get("type").map(|x| x.as_str());
		if let Some(Some(guid)) = json.get("guid").map(|x| x.as_str().map(|x| x.to_owned()));
		if let Some(Some(title)) = json.get("title").map(|x| x.as_str().map(|x| x.to_owned()));
		if let Some(Some(index)) = json.get("index").map(|x| x.as_u64());
		if let Some(Some(date_added)) = json.get("dateAdded").map(|x| x.as_i64());
		if let Some(date_added) = NaiveDateTime::from_timestamp_opt(date_added / 1_000_000, (date_added % 1_000_000) as u32 * 1000);
		if let Some(Some(last_modified)) = json.get("lastModified").map(|x| x.as_i64());
		if let Some(last_modified) = NaiveDateTime::from_timestamp_opt(last_modified / 1_000_000, (last_modified % 1_000_000) as u32 * 1000);
		if let Some(Some(id)) = json.get("id").map(|x| x.as_u64());
		if let root = json.get("root").map(|x| x.as_str()).and_then(|x| x.map(|x| x.to_owned()));
		if let Some(children) = {
			if let Some(Some(children)) = json.get("children").map(|x| x.as_array()) {
				let mut children_parsed = Vec::new();
				for c in children.into_iter() {
					if let Some(x) = get_entity(c) {
						children_parsed.push(x);
					} else {
						// too bad
					}
				}
				Some(children_parsed)
			} else {
				Some(vec![])
			}
		};
		then {
			Some(PlaceContainer {
				guid, title, index, date_added, last_modified, id, children, root
			})
		} else {
			None
		}
	}
}

fn get_entity(json: &Value) -> Option<Entity> {
	match json.get("type").map(|x| x.as_str()) {
		Some(Some("text/x-moz-place")) => Some(Entity::Place(get_place(json)?)),
		Some(Some("text/x-moz-place-container")) => Some(Entity::Container(get_container(json)?)),
		_ => None
	}
}

fn get_place(json: &Value) -> Option<Place> {
	if_chain! {
		if Some(Some("text/x-moz-place")) == json.get("type").map(|x| x.as_str());
		if let Some(Some(guid)) = json.get("guid").map(|x| x.as_str().map(|x| x.to_owned()));
		if let Some(Some(title)) = json.get("title").map(|x| x.as_str().map(|x| x.to_owned()));
		if let Some(Some(index)) = json.get("index").map(|x| x.as_u64());
		if let Some(Some(date_added)) = json.get("dateAdded").map(|x| x.as_i64());
		if let Some(date_added) = NaiveDateTime::from_timestamp_opt(date_added / 1_000_000, (date_added % 1_000_000) as u32 * 1000);
		if let Some(Some(last_modified)) = json.get("lastModified").map(|x| x.as_i64());
		if let Some(last_modified) = NaiveDateTime::from_timestamp_opt(last_modified / 1_000_000, (last_modified % 1_000_000) as u32 * 1000);
		if let Some(Some(id)) = json.get("id").map(|x| x.as_u64());
		if let annos = json.get("annos").cloned();
		if let Some(Some(uri)) = json.get("uri").map(|x| x.as_str().map(|x| x.to_owned()));
		then {
			Some(Place {
				guid, title, index, date_added, last_modified, id, annos, uri
			})
		} else {
			None
		}
	}
}